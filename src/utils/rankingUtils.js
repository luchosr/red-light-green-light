export const transformArray = (originalArray) => {
	return originalArray.map(({ playerName, highScore }) => ({
		playerName,
		highScore,
	}));
};
