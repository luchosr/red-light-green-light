export const VALIDATION_ERROR_MESSAGE =
	'Incorrect format, player names can only contain letters and/or numbers';

export const HOME_MAIN_TITLE = 'Create new player or log in';

export const HOME_BUTTON_TEXT = 'Register';

export const NAV_BAR_MAIN_TEXT = 'Hello';

export const GAME_VIEW_MAIN_TITLE = 'Hight Score:';

export const GAME_VIEW_MAIN_SUBTITLE = 'Score:';

export const GAME_VIEW_LEFT_BUTTON = 'Left';

export const GAME_VIEW_RIGHT_BUTTON = 'Right';

export const RED_COLOR = '#FF0000';

export const GREEN_COLOR = '#008000';

export const GREEN_LIGHT_MAX_VALUE = 10000;

export const GREEN_LIGHT_MIN_VALUE = 2000;

export const SCORE_VARIABLE_FACTOR = 100;

export const RANDOM_INTERVAL_MAX_VALUE = 1500;

export const RANDOM_INTERVAL_MIN_VALUE = -1500;

// agrupar por temáticas
