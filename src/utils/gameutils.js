import {
	persistCurrentPLayer,
	persistListOfPlayers,
} from '../services/localStorageServices';

import {
	GREEN_LIGHT_MAX_VALUE,
	GREEN_LIGHT_MIN_VALUE,
	RANDOM_INTERVAL_MAX_VALUE,
	RANDOM_INTERVAL_MIN_VALUE,
	SCORE_VARIABLE_FACTOR,
} from './textUtils';

// User utils:

export const validateName = (name) => {
	// regular expression for names validation with letters and numbers
	const regex = /^[a-zA-Z0-9]+$/;
	return regex.test(name);
};

export const createNewPlayerOrLogIn = (playerName, playersList) => {
	const existingUser = checkForExistingUser(playersList, playerName);

	if (!existingUser) {
		const newPlayer = { playerName, score: 0, highScore: 0 };

		const updatedPlayersList = [...playersList, newPlayer];

		persistCurrentPLayer(newPlayer);

		persistListOfPlayers(updatedPlayersList);
	} else {
		persistCurrentPLayer(existingUser);
	}
};

// Game utils:
export const greenLightTimeCalculator = (playerScore) =>
	Math.max(
		GREEN_LIGHT_MAX_VALUE - playerScore * SCORE_VARIABLE_FACTOR,
		GREEN_LIGHT_MIN_VALUE
	) + Math.random(RANDOM_INTERVAL_MIN_VALUE, RANDOM_INTERVAL_MAX_VALUE);

export const checkForExistingUser = (users, userName) => {
	const existingUser = users.find((user) => user.playerName === userName);
	return existingUser;
};

export const filterPlayersByName = (playersArray, playerName) => {
	return playersArray.filter((player) => player.playerName !== playerName);
};
