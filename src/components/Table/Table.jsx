import React from 'react';
import { PropTypes } from 'prop-types';
import { Box, Button, Typography } from '@mui/material';

export const Table = ({ headings, players }) => {
	// Ordenar la lista de jugadores por high score de forma descendente
	const sortedPlayers = players.sort((a, b) => b.highScore - a.highScore);

	return (
		<Box component="section">
			<Box component="table">
				<Box component="thead">
					<Box component="tr">
						{headings.map((heading, index) => (
							<Box key={index} component="th">
								{heading}
							</Box>
						))}
					</Box>
				</Box>
				<Box component="tbody">
					{/* {sortedPlayers.map((player, index) => (
						<Box component="tr" key={index}>
							<Box component="td">{index + 1}</Box>
							<Box component="td">{player.name}</Box>
							<Box component="td">{player.highScore}</Box>
						</Box>
					))} */}
				</Box>
			</Box>
		</Box>
	);
};

Table.propTypes = {
	headings: PropTypes.array,
	players: PropTypes.array,
};
