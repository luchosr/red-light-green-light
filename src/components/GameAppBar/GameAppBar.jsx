import * as React from 'react';
import PropTypes from 'prop-types';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import LogoutIcon from '@mui/icons-material/Logout';
import { NAV_BAR_MAIN_TEXT } from '../../utils/textUtils';

export const GameAppBar = ({ playerName, handleLogOutButton }) => {
	return (
		<Box sx={{ flexGrow: 1 }}>
			<AppBar position="static">
				<Toolbar>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						{`${NAV_BAR_MAIN_TEXT} ${playerName}`}
					</Typography>
					<IconButton
						size="large"
						edge="start"
						color="inherit"
						aria-label="menu"
						sx={{ mr: 0 }}
						onClick={handleLogOutButton}
					>
						<LogoutIcon />
					</IconButton>
				</Toolbar>
			</AppBar>
		</Box>
	);
};

GameAppBar.propTypes = {
	playerName: PropTypes.string,
	handleLogOutButton: PropTypes.func,
};
