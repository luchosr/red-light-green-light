import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { GameAppBar } from './GameAppBar';

describe('GameAppBar', () => {
  it('renders without crashing', () => {
    const playerName = 'John';
    render(<GameAppBar playerName={playerName} handleButtonClick={() => {}} />);
  });

  it('renders the component with player name', () => {
    const playerName = 'John';
    render(<GameAppBar playerName={playerName} handleButtonClick={() => {}} />);

    // Verificar que el componente renderice correctamente con el nombre del jugador
    expect(screen.getByText(`Hello ${playerName}`)).toBeInTheDocument();
  });

  it('calls handleButtonClick when IconButton is clicked', () => {
    const handleButtonClickMock = jest.fn();
    render(
      <GameAppBar playerName="John" handleButtonClick={handleButtonClickMock} />
    );

    // Simular un clic en el IconButton
    fireEvent.click(screen.getByLabelText('menu'));

    // Verificar que la función handleButtonClick fue llamada
    expect(handleButtonClickMock).toHaveBeenCalled();
  });

  it('renders the correct heading', () => {
    render(<GameAppBar />);
    const helloHeading = screen.getByText(/Hello/i);

    expect(helloHeading).toBeInTheDocument();
  });
});
