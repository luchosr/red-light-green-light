import React from 'react';
import { render, screen } from '@testing-library/react';
import { App } from './App';

describe('App', () => {
	test('renders Home view by default', () => {
		render(<App initialEntries={['/home']} />);
		// Verificar que la vista Home se renderice por defecto
		expect(screen.getByText(/Create new player/i)).toBeInTheDocument();
	});
});
