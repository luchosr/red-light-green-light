import React from 'react';

import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { Home } from './views/Home/Home';
import { Game } from './views/Game/Game';

import { RouterLayout } from './components/RouterLayout';
import { Ranking } from './views/Ranking/Ranking';

export const App = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<RouterLayout />}>
					<Route index element={<Home />} />
					<Route path="game" element={<Game />} />
					<Route path="rank" element={<Ranking />} />
					<Route path="*" element={<Home />} />
				</Route>
			</Routes>
		</BrowserRouter>
	);
};
