export const persistCurrentPLayer = (currentPlayerDataObject) => {
	window.localStorage.setItem(
		'currentPlayer',
		JSON.stringify([currentPlayerDataObject])
	);
};

export const persistListOfPlayers = (playersListArray) => {
	window.localStorage.setItem(
		'red-light-green-light',
		JSON.stringify(playersListArray)
	);
};

export const getListOfPlayers = () => {
	const listOfPlayers = JSON.parse(
		window.localStorage.getItem('red-light-green-light')
	);
	return listOfPlayers;
};

export const getCurrentPlayer = () => {
	const currentPlayer = JSON.parse(
		window.localStorage.getItem('currentPlayer')
	);
	return currentPlayer[0];
};
