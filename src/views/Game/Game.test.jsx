import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Game } from './Game';
import * as localStorageServices from '../../services/localStorageServices';
import * as gameUtils from '../../utils/gameUtils';

jest.mock('../../services/localStorageServices', () => ({
	getListOfPlayers: jest.fn(),
	getCurrentPlayer: jest.fn(),
	persistListOfPlayers: jest.fn(),
}));

jest.mock('../../utils/gameUtils', () => ({
	greenLightTimeCalculator: jest.fn(),
	filterPlayersByName: jest.fn(),
}));

describe('Game Component', () => {
	beforeEach(() => {
		window.localStorage.clear();
		jest.clearAllMocks();
	});

	test('renders Game component with initial state', () => {
		render(
			<MemoryRouter>
				<Game />
			</MemoryRouter>
		);

		expect(screen.getByText(/High Score/i)).toBeInTheDocument();
		expect(screen.getByText(/Score/i)).toBeInTheDocument();
		expect(screen.getByRole('button', { name: /Left/i })).toBeInTheDocument();
		expect(screen.getByRole('button', { name: /Right/i })).toBeInTheDocument();
	});

	test('handles button click during red light', async () => {
		gameUtils.greenLightTimeCalculator.mockReturnValue(5000);
		gameUtils.filterPlayersByName.mockReturnValue([]);

		render(
			<MemoryRouter>
				<Game />
			</MemoryRouter>
		);

		fireEvent.click(screen.getByRole('button', { name: /Left/i }));

		await waitFor(() => {
			expect(localStorageServices.persistListOfPlayers).not.toHaveBeenCalled();
			expect(screen.getByText(/Score: 0/i)).toBeInTheDocument();
		});
	});

	test('handles button click during green light', async () => {
		gameUtils.greenLightTimeCalculator.mockReturnValue(5000);
		gameUtils.filterPlayersByName.mockReturnValue([]);

		render(
			<MemoryRouter>
				<Game />
			</MemoryRouter>
		);

		fireEvent.click(screen.getByRole('button', { name: /Left/i }));

		await waitFor(() => {
			expect(localStorageServices.persistListOfPlayers).toHaveBeenCalled();
			// Ensure the score has been updated correctly based on the button click logic
			expect(screen.getByText(/Score: 1/i)).toBeInTheDocument();
		});
	});

	test('handles button click with same direction during green light', async () => {
		gameUtils.greenLightTimeCalculator.mockReturnValue(5000);
		gameUtils.filterPlayersByName.mockReturnValue([]);

		render(
			<MemoryRouter>
				<Game />
			</MemoryRouter>
		);

		fireEvent.click(screen.getByRole('button', { name: /Left/i }));

		await waitFor(() => {
			fireEvent.click(screen.getByRole('button', { name: /Left/i }));
			expect(localStorageServices.persistListOfPlayers).toHaveBeenCalled();
			// Ensure the score has been updated correctly based on the button click logic
			expect(screen.getByText(/Score: 0/i)).toBeInTheDocument();
		});
	});

	// Add more tests as needed for different scenarios and interactions
});
