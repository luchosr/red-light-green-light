import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { Box, Button, Typography } from '@mui/material';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import TrafficIcon from '@mui/icons-material/Traffic';

import { GameAppBar } from '../../components/GameAppBar/GameAppBar';

import {
	greenLightTimeCalculator,
	filterPlayersByName,
} from '../../utils/gameUtils';

import {
	GAME_VIEW_MAIN_TITLE,
	GAME_VIEW_MAIN_SUBTITLE,
	GAME_VIEW_LEFT_BUTTON,
	GAME_VIEW_RIGHT_BUTTON,
	RED_COLOR,
	GREEN_COLOR,
} from '../../utils/textUtils';

import {
	getListOfPlayers,
	getCurrentPlayer,
	persistListOfPlayers,
} from '../../services/localStorageServices';

export const Game = () => {
	const [color, setColor] = useState(RED_COLOR);
	const [currentPlayerData, setCurrentPlayerData] = useState([]);
	const [matchScore, setMatchScore] = useState(0);
	const [playerHighScore, setPlayerHighScore] = useState(0);
	const [playersList, setPlayersList] = useState([]);
	const [previousStep, setPreviousStep] = useState('');

	const navigate = useNavigate();

	useEffect(() => {
		// TODO: englobar todo en una función
		const listOfPlayers = getListOfPlayers();
		if (listOfPlayers) {
			setPlayersList(listOfPlayers);
		}

		const currentPlayer = getCurrentPlayer();
		if (currentPlayer) {
			setCurrentPlayerData(currentPlayer);
			setMatchScore(currentPlayer?.score);
			setPlayerHighScore(currentPlayer?.highScore);
		}
	}, []);

	useEffect(() => {
		setTimeout(() => {
			setColor(GREEN_COLOR);
		}, 3000);
		// TODO: poner los numeros en ficheros de constantes

		setInterval(() => {
			setTimeout(() => {
				setColor(GREEN_COLOR);
			}, 3000);

			setColor(RED_COLOR);
		}, greenLightTimeCalculator(matchScore));

		// empieza en rojo 3 segundos
		// setInterval(() => {
		// 	setTimeout(() => {
		// 		setColor(GREEN_COLOR);
		// 	}, 5000);
		// 	setTimeout(() => {
		// 		setColor(RED_COLOR);
		// 	}, 3000);
		// }, 5000);

		// setInterval(
		// 	() => {
		// 		setColor(GREEN_COLOR);
		// 		setTimeout(() => {
		// 			setColor(RED_COLOR);
		// 		}, greenLightTimeCalculator(matchScore));
		// 	},
		// 	greenLightTimeCalculator(matchScore) - 3000
		// );
		// get time between color change
	}, []);

	const handleButtonClick = (step) => {
		// If the traffic light is red, the player loses all points
		if (color === RED_COLOR) {
			setMatchScore(0);
			setPreviousStep(step);

			// TODO: pensar en unificar significados direction y step
		} else {
			// If the traffic light is green
			if (step !== previousStep && color === GREEN_COLOR) {
				// If the player takes a step in the opposite direction to the previous one, one point is added.
				setMatchScore((prevScore) => prevScore + 1);

				// If score upraises the high score, it will update
				if (matchScore === playerHighScore)
					setPlayerHighScore(playerHighScore + 1);

				setPreviousStep(step);

				// TODO: simplificar la logica que ya estaría en el color verde (solo else)
			} else {
				// If the player takes a step in the same direction as the previous one, one point is subtracted (if he has at least one)
				setMatchScore((prevScore) => (prevScore > 0 ? prevScore - 1 : 0));
				setPreviousStep(step);
			}
		}
	};

	const handleButtonOnLogOut = (e) => {
		e.preventDefault();

		// TODO: unificar esta función en una sola (update user)
		const currentPlayerName = currentPlayerData?.playerName;

		const filteredPlayers = filterPlayersByName(playersList, currentPlayerName);

		const updatedPlayer = {
			playerName: currentPlayerName,
			score: matchScore,
			highScore: playerHighScore,
		};

		const newArrayOfPlayers = [...filteredPlayers, updatedPlayer];

		persistListOfPlayers(newArrayOfPlayers);

		navigate('/');
	};

	return (
		<>
			<GameAppBar
				playerName={currentPlayerData?.playerName}
				handleLogOutButton={handleButtonOnLogOut}
			/>
			<Box component="main">
				<Typography variant="h5" component="h1">
					{`${GAME_VIEW_MAIN_TITLE} ${playerHighScore}`}
				</Typography>

				<TrafficIcon
					style={{ width: '100px', height: '100px', backgroundColor: color }}
				/>

				<Typography variant="h5" component="h2">
					{`${GAME_VIEW_MAIN_SUBTITLE} ${matchScore}`}
				</Typography>
				<Button
					variant="outlined"
					startIcon={<ArrowUpwardIcon />}
					onClick={() => handleButtonClick('left')}
				>
					{GAME_VIEW_LEFT_BUTTON}
				</Button>
				<Button
					variant="outlined"
					startIcon={<ArrowUpwardIcon />}
					onClick={() => handleButtonClick('right')}
				>
					{GAME_VIEW_RIGHT_BUTTON}
				</Button>
			</Box>
		</>
	);
};
