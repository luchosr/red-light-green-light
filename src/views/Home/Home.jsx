import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { TextField, Box, Button, Typography } from '@mui/material';

import { createNewPlayerOrLogIn, validateName } from '../../utils/gameUtils';

import {
	HOME_BUTTON_TEXT,
	HOME_MAIN_TITLE,
	VALIDATION_ERROR_MESSAGE,
} from '../../utils/textUtils';

import { getListOfPlayers } from '../../services/localStorageServices';

export const Home = () => {
	const [playerName, setPlayerName] = useState('');
	const [playersList, setPlayersList] = useState([]);
	const [error, setError] = useState({ error: false, message: '' });

	const navigate = useNavigate();

	useEffect(() => {
		const listOfPlayers = getListOfPlayers();

		if (listOfPlayers) {
			setPlayersList(listOfPlayers);
		}
	}, []);

	const handleSubmit = (e) => {
		e.preventDefault();

		if (!validateName(playerName)) {
			setError({
				error: true,
				message: VALIDATION_ERROR_MESSAGE,
			});

			return;
		}

		createNewPlayerOrLogIn(playerName, playersList, setPlayersList);

		navigate('/game');
	};

	return (
		<>
			<Typography variant="h5" component="h1">
				{HOME_MAIN_TITLE}
			</Typography>

			<Box component="form" onSubmit={handleSubmit}>
				<TextField
					required
					fullWidth
					type="text"
					label="Name"
					id="playerName"
					variant="outlined"
					value={playerName}
					error={error.error}
					onChange={(e) => setPlayerName(e.target.value)}
					helperText={error.message}
				/>

				<Button type="submit" variant="outlined" sx={{ mt: 2 }}>
					{HOME_BUTTON_TEXT}
				</Button>
			</Box>
		</>
	);
};
