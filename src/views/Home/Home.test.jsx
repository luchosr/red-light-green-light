import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Home } from './Home';
import * as localStorageServices from '../../services/localStorageServices';
import * as gameUtils from '../../utils/gameUtils';

jest.mock('../../services/localStorageServices', () => ({
	getListOfPlayers: jest.fn(),
	persistCurrentPLayer: jest.fn(),
	persistListOfPlayers: jest.fn(),
}));

jest.mock('../../utils/gameUtils', () => ({
	checkForExistingUser: jest.fn(),
	validateName: jest.fn(),
}));

describe('Home Component', () => {
	beforeEach(() => {
		window.localStorage.clear();
	});

	test('renders Home component with initial state', () => {
		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		expect(screen.getByText(/Create new player/i)).toBeInTheDocument();
		expect(screen.getByLabelText(/Name/i)).toBeInTheDocument();
		expect(
			screen.getByRole('button', { name: /Register/i })
		).toBeInTheDocument();
	});

	test('handles form submission with valid name and new user', async () => {
		gameUtils.validateName.mockReturnValue(true);
		gameUtils.checkForExistingUser.mockReturnValue(null);

		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		fireEvent.change(screen.getByLabelText(/Name/i), {
			target: { value: 'John' },
		});
		fireEvent.submit(screen.getByRole('button', { name: /Register/i }));

		await waitFor(() => {
			expect(localStorageServices.persistCurrentPLayer).toHaveBeenCalled();
			expect(localStorageServices.persistListOfPlayers).toHaveBeenCalled();
		});
	});

	test('handles form submission with valid name and existing user', async () => {
		gameUtils.validateName.mockReturnValue(true);
		gameUtils.checkForExistingUser.mockReturnValue({
			playerName: 'John',
			score: 0,
			highScore: 0,
		});

		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		fireEvent.change(screen.getByLabelText(/Name/i), {
			target: { value: 'John' },
		});
		fireEvent.submit(screen.getByRole('button', { name: /Register/i }));

		await waitFor(() => {
			expect(localStorageServices.persistCurrentPLayer).toHaveBeenCalled();
			expect(localStorageServices.persistListOfPlayers).not.toHaveBeenCalled();
		});
	});

	test('handles form submission with invalid name', async () => {
		gameUtils.validateName.mockReturnValue(false);
		window.localStorage.clear();

		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		fireEvent.change(screen.getByLabelText(/Name/i), {
			target: { value: 'Invalid!@#' },
		});
		fireEvent.submit(screen.getByRole('button', { name: /Register/i }));

		await waitFor(() => {
			expect(localStorageServices.persistCurrentPLayer).not.toHaveBeenCalled();
			expect(localStorageServices.persistListOfPlayers).not.toHaveBeenCalled();
		});
	});
});
