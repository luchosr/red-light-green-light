import React, { useEffect, useState } from 'react';
import { Box, Typography } from '@mui/material';
import { Table } from '../../components/Table/Table';
import { getListOfPlayers } from '../../services/localStorageServices';
import { transformArray } from '../../utils/rankingUtils';

export const Ranking = () => {
	const [playersList, setPlayersList] = useState();

	useEffect(() => {
		const listOfPlayers = getListOfPlayers();
		if (listOfPlayers) {
			const formattedArrayOfPlayers = transformArray(listOfPlayers);
			setPlayersList(formattedArrayOfPlayers);
			console.log('el player list es: ', playersList);
		}
	}, []);

	return (
		<Box component="main">
			<Typography variant="h4" component="h1" sx={{ mt: 2, mb: 2 }}>
				Players Rank
			</Typography>

			{playersList && (
				<Table
					headings={['Position', 'Player Name', 'High Score']}
					rows={playersList}
				/>
			)}
			{/* <Table
				headings={['Position', 'Player Name', 'High Score']}
				rows={playersList}
			/> */}
		</Box>
	);
};
